(function() {

	var limitCount = 0;
	var maxLimitCount = 10;
	var targetFunction = [];

/*
	Backbone.js
	Models section
*/

	var Func = Backbone.Model.extend();

	var Limit = Backbone.Model.extend({

		defaults: {
			'parameters': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
			'comparator': 'ge',
			'b': 0
		}

	});

/*
	Backbone.js
	Views section
*/

	var MainView = Backbone.View.extend({
		el: $('body'),
		events: {
			'click #start': 'startSimplex',
			'click #reset': 'resetWindow',
			'click .precision-changer.fa-chevron-circle-up': 'incPrecision',
			'click .precision-changer.fa-chevron-circle-down': 'decPrecision',
			'click .iteration-limit-changer.fa-chevron-circle-up': 'incIterationLimit',
			'click .iteration-limit-changer.fa-chevron-circle-down': 'incIterationLimit10',
			'click .iteration-limit-changer.fa-arrow-circle-up': 'decIterationLimit',
			'click .iteration-limit-changer.fa-arrow-circle-down': 'decIterationLimit10',
			'click #delete-boundaries': 'deleteBoundaries'
		},
		startSimplex: function() {
			this.changeDisplayMode();

			var A = [];
			var b = [];
			var c = [];
			listView.collection.each(function(limit) {
					if(limit.get('comparator') == 'le') {
						A.push(limit.get('parameters'));
						b.push(limit.get('b'));
					} else {
						A.push(limit.get('parameters').map(function(x) {
							return x ? -x : 0;
						}));
						b.push(limit.get('b') ? -limit.get('b') : 0);
					}
			});

			setTimeout(function(){
				var preparedTables = prepareTable(A, b, targetFunction);
				A = preparedTables.A;
				b = preparedTables.b;
				c = preparedTables.c;

				var start = new Date().getTime();
				var result = startCalculating(A, b, c);
				var calculatingEnd = new Date().getTime();
				writeInConsole("Czas wykonania algorytmu simpleks: " + (calculatingEnd - start) + "ms<br>");
				
				if(result) {
					var resultValue = result[0]["value"];
				} else {
					var resultValue = "brak rozwiązania PL";
				}

				if(!isNaN(resultValue)) {
					var resultBBValue = startBB(result, A, b, c);
					writeInConsole("Czas wykonania algorytmu B&B: " + (new Date().getTime() - calculatingEnd) + "ms");
					if(resultBBValue == null) {
						var resultBBValue = "brak rozwiązania PCL";
					}
				}

				$('[data-toggle="popover"]').popover();

				var consoleBox = $('#console > .panel-body');
				consoleBox.scrollTop(consoleBox[0].scrollHeight);

				$('#results > .panel-body > i').removeClass('fa-spin').fadeOut(100, function() {
					$('#results > .panel-body').text('Wynik PL: ' + resultValue);
					$('#results > .panel-body').append('<br>Wynik PCL: ' + resultBBValue);
				});
			}, 500);
		},
		resetWindow: function() {
			location.reload(true);
		},
		incPrecision: function() {
			var value = $('#precision-value').html();
			if(value < 9) {
				$('#precision-value').text(++value);
			}
		},
		decPrecision: function() {
			var value = $('#precision-value').html();
			if(value > 1) {
				$('#precision-value').text(--value);
			}
		},
		incIterationLimit: function() {
			var value = $('#iteration-limit-value').html();
			if(value < 100) {
				$('#iteration-limit-value').text(++value);
			}
		},
		incIterationLimit10: function() {
			var value = $('#iteration-limit-value').html();
			if(value > 10) {
				$('#iteration-limit-value').text(--value);
			}
		},
		decIterationLimit: function() {
				var value = $('#iteration-limit-value').html();
			if(value < 91) {
				$('#iteration-limit-value').text(parseInt(value) + 10);
			}
		},
		decIterationLimit10: function() {
			var value = $('#iteration-limit-value').html();
			if(value > 11) {
				$('#iteration-limit-value').text(parseInt(value) - 10);
			}
		},
		changeDisplayMode: function() {
			$('#start').attr('disabled', 'disabled');
			$('#main-column').removeClass('col-md-12').addClass('col-md-7');
			$('#sidebar').removeClass('col-md-12').addClass('col-md-5');
			$('.main-column-panels').removeClass('col-md-6').addClass('col-md-12');

			$('#console').removeClass('hidden');
			$('#results').removeClass('hidden');
			$('#reset-panel').removeClass('hidden');

			$('#set-function').hide();
			$('#add-boundary').hide();
			$('#start-panel').hide();
			$('.remove-limit').hide();
			$('.precision-changer').hide();
			$('.iteration-limit-changer').hide();
		}
	});

	var LimitView = Backbone.View.extend({

		tagName: 'li',
		events: {
			'click .remove-limit': 'remove',
		},
		initialize: function() {
			_.bindAll(this, 'render', 'remove');
			this.model.on('change', this.render, this);
		},
		render: function(e) {
			var boundary = '';
			var isFirst = true;

			for(var i = 1; i <= 10; i++) {
				var parameter = this.model.get('parameters')[i-1];

				if(parameter) {
					if(parameter > 0 && i > 1 && !isFirst) {
						boundary += '+ '
					} else {
						isFirst = false;
					}

					if(parameter < 0) {
						boundary += '- '
					}

					if(Math.abs(parameter) == 1) {
						boundary += 'x<sub>' + i + '</sub> ';
					} else {
						boundary += Math.abs(parameter) + 'x<sub>' + i + '</sub> ';
					}
				}
			}

			if(this.model.get('comparator') == 'le') {
				boundary += ' &le; '
			} else {
				boundary += ' &ge; '
			}

			boundary += this.model.get('b');

			$('#start').removeAttr('disabled');
			this.$el.addClass('list-group-item').html(boundary + '<div class="pull-right comment-info comment-hover remove-limit"><i class="fa fa-minus"></i> usuń</div>');
			return this;
		},
		remove: function() {
			$(this.el).remove();
			$('#limit-count').text(--limitCount);
			if(limitCount < maxLimitCount) {
				$('#add-boundary').show();
			}
			if(limitCount < 1) {
				$('#start').attr('disabled', 'disabled');
			}
			this.model.destroy();
		},
		hover: function() {
			this.$el.find('.remove-limit').toggleClass('hidden');
		}

	});

	var LimitList = Backbone.Collection.extend({
		model: Limit,
		url: 'boundaries.json',
	});

	var LimitListView = Backbone.View.extend({

		el: $('#boundaries-list'),
		initialize: function() {
			this.collection = new LimitList();
			this.collection.bind('add', this.append, this);
			this.collection.fetch();
		},
		append: function(limit) {
			var limitView = new LimitView({
				model: limit
			});

			this.$el.append(limitView.render().el);
			$('#limit-count').text(++limitCount);
			if(limitCount >= maxLimitCount) {
				$('#add-boundary').hide();
			}
		},
		add: function(parameters, comparator, b) {
			var limit = new Limit();

			limit.set({
				parameters: parameters,
				comparator: comparator,
				b: b,
			});

			this.collection.add(limit);
		},
	});

	var FunctionView = Backbone.View.extend({

		el: $('#function-span'),
		initialize: function() {
			var self = this;
			$.getJSON("target.json", function( data ) {
				targetFunction = data[0].target;
				self.render();
			});
		},
		render: function(e) {
			var func = '';
			var isFirst = true;

			for(var i = 1; i <= 10; i++) {
				var parameter = targetFunction[i-1];

				if(parameter) {
					if(parameter > 0 && i > 1 && !isFirst) {
						func += '+ '
					} else {
						isFirst = false;
					}

					if(parameter < 0) {
						func += '- '
					}

					if(Math.abs(parameter) == 1) {
						func += 'x<sub>' + i + '</sub> ';
					} else {
						func += Math.abs(parameter) + 'x<sub>' + i + '</sub> ';
					}
				}
			}

			$('#start').removeClass('disabled');
			$('#set-function-label').html('wprowadź inne');
			this.$el.html(func);
			return this;
		},

	});

/*
	Backbone.js
	Views section -> modals
*/

	var SetFunctionModal = Backbone.View.extend({

		el: $('#set-function-modal'),
		events: {
			'click .add-new': 'add'
		},
		initialize: function(options) {
			this.functionView = options.functionView;
		},
		add: function() {
			var parameters = [];
			var err = false;
			var isFilled = false;
			
			for(var i = 1; i <= 10; i++) {
				var parameter = $('#set-function-modal [name="x' + i + '"]').val();
				if(parameter > 0 || parameter < 0) {
					isFilled = true;
				}

				if(!parameter) {
					parameters.push(0);
				} else if(!isNaN(parameter)) {
					parameters.push(parameter);
				} else {
					err = true;
					break;
				}
			}

			if(!err && isFilled) {
				targetFunction = parameters;
				this.functionView.render();
				this.$el.modal('hide');
				this.$el.find('.alert').addClass('hidden');
				$('input.vector-form').val('');
			} else {
				this.$el.find('.alert').removeClass('hidden');
			}
		},

	});

	var AddLimitModal = Backbone.View.extend({

		el: $('#add-limit-modal'),
		events: {
			'click .add-new': 'add'
		},
		initialize: function(options) {
			this.listView = options.listView;
		},
		add: function() {
			var parameters = [];
			var err = false;
			var isFilled = false;
			
			for(var i = 1; i <= 10; i++) {
				var parameter = $('#add-limit-modal [name="x' + i + '"]').val();
				if(parameter > 0 || parameter < 0) {
					isFilled = true;
				}

				if(!parameter) {
					parameters.push(0);
				} else if(!isNaN(parameter)) {
					parameters.push(parameter);
				} else {
					err = true;
					break;
				}
			}

			var comp = $('#add-limit-modal [name="comparator"]:checked').val();
			var b = $('#add-limit-modal [name="b"]').val();

			if(!b) {
				err = true;
			}

			if(!err && isFilled) {
				this.listView.add(parameters, comp, (b));
				this.$el.modal('hide');
				this.$el.find('.alert').addClass('hidden');
				$('input.vector-form').val('');
			} else {
				this.$el.find('.alert').removeClass('hidden');
			}
		},

	});

/*
	Backbone.js
	Declarations
*/

	var mainView = new MainView();
	var functionView = new FunctionView();
	var listView = new LimitListView();

	var setFunctionModal = new SetFunctionModal({
		functionView: functionView
	});

	var addLimitModal = new AddLimitModal({
		listView: listView
	});

})();

Array.prototype.equals = function(array) {
	if(!array){
		return false;
	}

	if(this.length != array.length){
		return false;
	}

	for (var i = 0, l=this.length; i < l; i++) {
		if(this[i] instanceof Array && array[i] instanceof Array) {
			if(!this[i].equals(array[i])) {
				return false;       
			}
		} else if(this[i] != array[i]) { 
			return false;   
		}           
	}       
	return true;
}   