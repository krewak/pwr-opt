// function checkIsInteger(value) {
// 	return !isNaN(value) && 
// 		parseInt(Number(value)) == value && 
// 		!isNaN(parseInt(value, 10));
// }

function checkIsInteger(value) {
	var e = Math.pow(10, -$('#precision-value').html());
	var fr = value - Math.floor(value);

	return Math.min(fr, 1 - fr) <= e;
}

function addBound(A, b, c, decisionVar, decisionVarIndex, type) {
	type = (type == 'lower') ? 1 : -1;

	A.forEach(function(value, index) {
		value.push(0);
	});

	var row = [];
	for(var i = 0; i < A[0].length - 1; i++) {
		row.push((i == decisionVarIndex) ? type : 0);
	}
	row.push(1);

	A.push(row);
	b.push(decisionVar * type)
	c.push(0);

	return {
		A: A, 
		b: b, 
		c: c
	};
}

function startBB(result, A, b, c) {

	var isIntegers = true;
	result.forEach(function(value) {
		if(!checkIsInteger(value["value"])) {
			isIntegers = false;
		}
	});

	var max = $('#iteration-limit-value').html();
	var L = 0;

	if(isIntegers) {
		writeInConsole("Wynik całkowtoliczbowy, nie uruchomiono procedury B&B.");
		return result[0]["value"];
	} else {
		writeInConsole("Uruchomiono procedurę B&B.");
	}

	var optimeResult;
	var resultsStack = [];
	resultsStack.push({
		value: result[0]["value"],
		result: result,
		A: A,
		b: b,
		c: c
	});

	var upperBound = Math.floor(result[0]["value"]);
	writeInConsole("Górne oszacowanie PCL: " + upperBound + "<br>");
	var resultValue = 0;

	var optimalResultFound = false;
	while(max - L > 0 && resultsStack.length > 0 && !optimalResultFound) {
		var node = resultsStack.shift();
		result = node.result;
		A = node.A;
		b = node.b;
		c = node.c;

		var decisionVar;
		for(var i = 1; i < result.length; i++) {
			if(result[i]["index"] <= (c.length - b.length) && !checkIsInteger(result[i]["value"])) {
				decisionVarIndex = result[i]["index"] - 1;
				decisionVar = result[i]["value"];

				var floor = Math.floor(decisionVar);
				var ceil = floor + 1;

				var Alower = [];
				var Aupper = [];
				A.forEach(function(row, index) {
					Alower.push(_.clone(row));
					Aupper.push(_.clone(row));
				});
				var blower = _.clone(b);
				var bupper = _.clone(b);
				var clower = _.clone(c);
				var cupper = _.clone(c);

				// lower bound
				writeInConsole("Iteracja #" + ++L);
				var tables = addBound(Alower, blower, clower, floor, decisionVarIndex, 'lower');
				var A1 = tables.A, b1 = tables.b, c1 = tables.c;
				var result1 = startCalculating(A1, b1, c1);

				if(result1) {
					var resultValue = result1[0]["value"];
					var isIntegers = true;
					result1.forEach(function(value) {
						if(value["index"] <= (c.length - b.length) && !checkIsInteger(value["value"])) {
							isIntegers = false;
						}
					});
					var actualNode = {
						value: resultValue,
						result: result1,
						A: A,
						b: b,
						c: c
					};
					if(!isIntegers) {
						resultsStack.push(actualNode);
					} else {
						if(optimeResult == null || actualNode.value > optimeResult.value) {
							optimeResult = actualNode;
						}
					}
				} else {
					var resultValue = "brak rozwiązania";
				}

				if(optimeResult && isIntegers) {
					if(optimeResult.value == upperBound) {
						optimalResultFound = true;
						writeInConsole("Wynik jest całkowtoliczbowy i równy górnemu oszacowaniu.");
						break;
					}
				}

				// upper bound
				writeInConsole("Iteracja #" + ++L);
				var tables = addBound(Aupper, bupper, cupper, ceil, decisionVarIndex, 'upper');
				var A2 = tables.A, b2 = tables.b, c2 = tables.c;
				var result2 = startCalculating(A2, b2, c2);

				if(result2) {
					var resultValue = result2[0]["value"];
					var isIntegers = true;
					result2.forEach(function(value) {
						if(value["index"] <= (c.length - b.length) && !checkIsInteger(value["value"])) {
							isIntegers = false;
						}
					});
					var actualNode = {
						value: resultValue,
						result: result2,
						A: A,
						b: b,
						c: c
					};
					if(!isIntegers) {
						resultsStack.push(actualNode);
					} else {
						if(optimeResult == null || actualNode.value > optimeResult.value) {
							optimeResult = actualNode;
						}
					}
				} else {
					var resultValue = "brak rozwiązania";
				}

				if(optimeResult && isIntegers) {
					if(optimeResult.value == upperBound) {
						optimalResultFound = true;
						writeInConsole("Wynik jest całkowtoliczbowy i równy górnemu oszacowaniu.");
						break;
					}
				}

			}
			// break;
		}
	}

	var values = [];
	if(optimeResult) {
		optimeResult.result.forEach(function(value, index) {
			if(index && value.index <= (c.length - b.length)) {
				values.push("x" + value.index + ": " + value.value);
			}
		});

		writeInConsole('Rozwiązanie PCL: ' + optimeResult.value + '; <a tabindex="0" data-html="true" data-toggle="popover" data-placement="left" data-trigger="focus" title="Wartości x" data-content="' + values.join("<br>") + '">zobacz wartości x</a><br>');
		return optimeResult.value;
	} else{
		writeInConsole('Nie znaleziono rozwiązania.');
		return null;
	}

}