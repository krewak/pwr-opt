function writeInConsole(text) {
	var consoleBox = $('#console > .panel-body');
	consoleBox.append("> " + text + "<br>");
}

function prepareTable(A, b, c) {

	// trimming tables
	var maxSize = 0;
	A.forEach(function(row, index) {
		A[index].forEach(function(value, column) {
			if(value != 0 && column > maxSize) {
				maxSize = column;
			}
		});
	});
	c.forEach(function(value, index) {
		if(value != 0 && index > maxSize) {
			maxSize = index;
		}
	});

	A.forEach(function(row, index) {
		A[index].forEach(function(value, column) {
			A[index].splice(maxSize + 1);
		});
	});
	c.splice(maxSize + 1);

	// adding eyes and zeros
	var m = A.length;
	var n = A[0].length;
	A.forEach(function(row, index) {
		for(var i = 0; i < m; i++) {
			A[index].push((i == index) ? 1 : 0);
		}
	});

	for(var i = 0; i < m; i++) {
		c.push(0);
	}

	return {
		A: A,
		b: b,
		c: c
	};
}

function makeSimplexTableau(A, b, c, x0) {
	var m = A.length;
	var n = A[0].length;

	var i = 0;
	var table = [];
	A.forEach(function(row) {
		var j = 0;
		table[i] = [];
		row.forEach(function(val) {
			table[i][j++] = val;
		});
		i++;
	});

	var i = 0;
	b.forEach(function(value) {
		table[i++][n] = value;
	});

	var i = 0;
	table[m] = [];
	c.forEach(function(value) {
		table[m][i++] = value;
	});

	table[m][n] = x0;

	return table;
}

function makeExpandedSimplexTableau(A, b, c, x0) {
	var m = A.length;
	var n = A[0].length;

	var i = 0;
	var table = [];
	A.forEach(function(row) {
		var j = 0;
		table[i] = [];
		row.forEach(function(val) {
			table[i][j] = val;
			table[i][j + m] = (i == j - 2) ? 1 : 0;
			j++;
		});
		i++;
	});

	var i = 0;
	b.forEach(function(value) {
		table[i++][n + m] = value;
	});

	var i = 0;
	table[m] = [];
	c.forEach(function(value) {
		table[m][i] = value;
		table[m][i + m] = 0;
		i++;
	});

	table[m][n + m] = x0;

	table[m + 1] = [];
	for(var i = 0; i < n ; i++) {
		table[m + 1][i] = 0;
	}
	for(var i = n; i < m + n ; i++) {
		table[m + 1][i] = -1;
	}
	table[m + 1][n + m] = 0;

	return table;
}

function getBaseColumns(A, baseIndexes) {
	var i = 0;
	var base = [];
	A.forEach(function(row) {
		var j = 1, k = 0;
		base[i] = [];
		row.forEach(function(val) {
			if(jQuery.inArray(j, baseIndexes) >= 0) {
				base[i][k++] = val;
			}
			j++;
		});
		i++;
	});
	return base;
}

function replaceRows(A, row1, row2) {
	var tempRow = A[row1];
	A[row1] = A[row2];
	A[row2] = A[tempRow];
	return A;
}

function columnSelect(A, b, col) {
	var eps = 0.0001;
	if(Math.abs(A[col][col]) < eps) {

		var maxIndex;
		if(Math.abs(A[col + 1][col]) < eps) {
			var max = null;
			for(var i = col; i < A.length; i++) {
				if (abs(A[i][col]) > max) {
					maxIndex = i;
					max = abs(A[i][col]);
				}
				maxIndex += col + 1;
			} 
		} else {
			maxIndex = col - 1;
		}

		A = replaceRows(A, col, maxIndex);
		b = replaceRows(b, col, maxIndex);
	}
	return {
		A: A,
		b: b
	};
}


function solveGaussian(A, b) {
	var m = A.length;
	var n = A[0].length;

	for(var col = 0; col < n - 1; col++) {
		var tables = columnSelect(A, b, col);
		A = tables.A
		b = tables.b

		for(var row = col + 1; row < n; row++) {
			var p = A[row][col] / A[col][col];
			for(var i = 0; i < n; i++) {
				A[row][i] -= A[col][i] * p;
			}
			b[row] -= b[col] * p;
		}
	}
	var x = b;
	x[n - 1] = x[n - 1]/A[n - 1][n - 1];

	for(var row = n - 2; row >= 0; row--) {
		for(var col = n - 1; col >= row + 1; col--) {
			x[row] -= A[row][col] * x[col];
		}
		x[row] /= A[row][row];
	}
	return x;
}

function checkLinearCombination(A, c) {
	var m = A.length;
	var n = A[0].length;

	for(var row = 0; row < m; row++) {
		var factor = A[row][0] / c[0];
		var isRowLinearCombined = true;

		for(var col = 0; col < n - m; col++) {
			if(A[row][col] / c[col] != factor) {
				isRowLinearCombined = false;
			}
		}

		if(isRowLinearCombined) {
			return true;
		}
	}
	return false;;
}

function startCalculating(A, b, c) {
	var x0 = 0;

	var m = A.length;
	var n = A[0].length;
	var s = _.range(n);

	var indexes = [];
	for(var i = 0; i < n; i++) {
		indexes[i] = i + 1;
	}

	var isLinearCombination = checkLinearCombination(A, c);

	// currently in base
	var j = 0;
	var baseIndexes = [];
	for(var i = n - m; i < n; i++) {
		baseIndexes[j++] = indexes[i];
	}

	// finding target value
	var j = 0;
	baseIndexes.forEach(function(index) {
		x0 += b[j] * c[index - 1];
	});

	// making simplex tableau
	var table = makeSimplexTableau(A, b, c, x0);

	// step 1
	var i = 0;
	var base = getBaseColumns(A, baseIndexes);
	var acceptableInitialSolution = solveGaussian(base, b);

	var smallerIndexes = [];
	var greaterIndexes = [];
	var optimize = false;

	for(var i = 0; i < m; i++) {
		if(acceptableInitialSolution[i] < 0) {
			writeInConsole("Rozpoczęto poszukiwanie rozwiązania bazowego.");
			greaterIndexes.push(i);
			optimize = true;
		} else {
			smallerIndexes.push(i);
		}
	}

	if(optimize) {
		table = makeExpandedSimplexTableau(A, b, c, x0);

		smallerIndexes.forEach(function(value, index) {
			smallerIndexes[index] = value + n;
		});
		
		smallerIndexes.forEach(function(value, indexIndex) {
			table.forEach(function(row, index) {
				table[index].splice(value - indexIndex, 1);
			});
		});
		
		smallerIndexes.forEach(function(value, index) {
			smallerIndexes[index] = value - n;
		});

		var n2 = n + m - smallerIndexes.length;
		var s = _.range(n2);
		var baseIndexes = [];
		for(var i = n; i < n2; i++) {
			baseIndexes.push(s[i]);
		}
		if(baseIndexes.length < m) {
			smallerIndexes.forEach(function(i) {
				for(var j = 0; j < n; j++) {
					if(table[i][j] != 0 && table[i][table[i].length - 1] / table[i][j] > 0) {
						baseIndexes.push(j);
						if(baseIndexes.length == m) {
							break;
						}
					}
				}
			});
		}

		if(baseIndexes.length < m) {
			writeInConsole("Wystąpił błąd.");
			return null;
		}

		greaterIndexes.forEach(function(i) {
			table[i].forEach(function(value, index) {
				table[table.length - 1][index] += table[i][index];
			});
		});

		while(true) {
			var p = [];
			for(var i = 0; i < n2; i++) {
				p.push(table[table.length - 1][i]);
			}
			var min = null, minIndex = null;
			p.forEach(function(value, index) {
				if(min == null || min < value) {
					min = value;
					minIndex = index;
				}
			});

			if(min <= 0) {
				writeInConsole("Wyznaczono początkowe rozwiązanie dopuszczalne.");

				acceptableInitialSolution = [];
				for(var row = 0; row < m; row++) {
					acceptableInitialSolution.push(table[row][n2]);
				}

				x0 = -table[table.length - 1][table[table.length - 1].length - 1];

				if(x0 > 0) {
					writeInConsole("Nie udało się wyznaczyć pierwszej bazy.");
					return null;
				} else {
					
					table.splice(-1, 1);
					
					var deletedColumnCounter = 0;
					for(var i = n; i < n2; i++) {
						table.forEach(function(value, row) {
							table[row].splice(i - deletedColumnCounter, 1);
						});
						deletedColumnCounter++;
					}
					s = _.range(n);
				}
				break;
			}
			

			var positives = [];
			for(var i = 0; i < m; i++) {
				if(table[i][minIndex] > 0) {
					positives.push(i);
				}
			}

			if(!positives.length) {
				writeInConsole("Zadanie nieograniczone.");
				return null;
			}

			var thetas = [];
			positives.forEach(function(value) {
				thetas.push(table[value][n2] / table[value][minIndex]);
			});


			var theta = null;
			thetas.forEach(function(value, index) {
				if(theta == null || theta > value) {
					theta = value;
				}
			});

			for(var i = 0; i < m; i++) {
				if(table[i][minIndex] > 0) {
					if (table[i][n2] / table[i][minIndex] == theta) {
						break;
					}
				}
			}

			var leavingRow = i;
			table[leavingRow].forEach(function(value, index) {
				table[leavingRow][index] /= table[leavingRow][minIndex];
			});

			for(var i = 0; i < m + 2; i++) {
				if(i == leavingRow) {
					continue;
				}
				var minForRow = table[i][minIndex];
				for(var j = 0; j < table[i].length; j++) {
					table[i][j] -= table[leavingRow][j] * minForRow;
				}
			}

			baseIndexes[leavingRow] = s[minIndex];

		}	// end while()
	}	// end optimze()

	while(true) {

		var p = [];
		for(var i = 0; i < n; i++) {
			p.push(table[m][i]);
		}

		var min = null, minIndex = null;
		p.forEach(function(value, index) {
			if(min == null || min < value) {
				min = value;
				minIndex = index;
			}
		});

		if(min <= 0) {
			writeInConsole("Znaleziono optimum.");
			if(isLinearCombination) {
				writeInConsole("Znalezione kombinację liniową, istnieje więcej niż jeden wynik.");
			}

			acceptableInitialSolution = [];
			for(var row = 0; row < m; row++) {
				acceptableInitialSolution.push(table[row][n]);
			}

			x0 = - table[table.length - 1][table[table.length - 1].length - 1];
			if(!isLinearCombination) {
				break;
			} else {
				var newBaseIndexes = _.clone(baseIndexes);
				var newTable = [];
				table.forEach(function(row, index) {
					newTable.push(_.clone(row));
				});
				var firstSolution = {
					table: newTable,
					baseIndexes: newBaseIndexes
				};
				isLinearCombination = false;
			}
		}

		var positives = [];
		for(var i = 0; i < m; i++) {
			if(table[i][minIndex] > 0) {
				positives.push(i);
			}
		}

		if(!positives.length) {
			writeInConsole("Zadanie nieograniczone.");
			return null;
		}

		var thetas = [];
		positives.forEach(function(value) {
			thetas.push(table[value][table[value].length - 1] / table[value][minIndex]);
		});

		var theta = null;
		thetas.forEach(function(value, index) {
			if(theta == null || theta > value) {
				theta = value;
			}
		});

		for(var i = 0; i < m; i++) {
			if(table[i][minIndex] > 0) {
				if (table[i][table[i].length - 1] / table[i][minIndex] == theta) {
					break;
				}
			}
		}

		var leavingRow = i;
		var denomiator = table[leavingRow][minIndex];
		table[leavingRow].forEach(function(value, index) {
			table[leavingRow][index] /= denomiator;;
		});

		for(var i = 0; i < m + 1; i++) {
			if(i == leavingRow) {
				continue;
			}
			var minForRow = table[i][minIndex];
			for(var j = 0; j < table[i].length; j++) {
				table[i][j] -= table[leavingRow][j] * minForRow;
			}
		}

		baseIndexes[leavingRow] = s[minIndex];
	}

	if(typeof firstSolution !== 'undefined') {
		solution = [];
		for(var i = 0; i < table.length; i++) {
			solution.push({
				index: (i != table.length - 1) ? baseIndexes[i] + 1 : 0,
				value: (i != table.length - 1) ? table[i][table[i].length - 1] : -table[i][table[i].length - 1],
			});
		}

		solution.sort(function(a, b) {
			return a.index - b.index;
		});

		var secondPointValues = [], conflict = false;
		solution.forEach(function(s, index) {
			if(s["value"] < 0) {
				writeInConsole("Zbiór pusty.");
				conflict = true;
				return null;
			}
			if(index && s["index"] <= (c.length - b.length)) {
				secondPointValues.push("x" + s["index"] + ": " + s["value"]);
			}
		});
		if(conflict) {
			return null;
		}

		table = firstSolution.table;
		baseIndexes = firstSolution.baseIndexes;
	}

	solution = [];
	for(var i = 0; i < table.length; i++) {
		solution.push({
			index: (i != table.length - 1) ? baseIndexes[i] + 1 : 0,
			value: (i != table.length - 1) ? table[i][table[i].length - 1] : -table[i][table[i].length - 1],
		});
	}

	solution.sort(function(a, b) {
		return a.index - b.index;
	});

	var values = [], conflict = false;
	solution.forEach(function(s, index) {
		if(s["value"] < 0) {
			writeInConsole("Zbiór pusty.");
			conflict = true;
			return null;
		}
		if(index && s["index"] <= (c.length - b.length)) {
			values.push("x" + s["index"] + ": " + s["value"]);
		}
	});
	if(conflict) {
		return null;
	}
	var statement = 'Rozwiązanie PL: ' + x0 + '; ';
	var points = '<a tabindex="0" data-html="true" data-toggle="popover" data-placement="left" data-trigger="focus" title="Wartości x" data-content="' + values.join("<br>") + '">zobacz wartości x</a>';
	if(typeof firstSolution !== 'undefined') {
		if(values.equals(secondPointValues)) {
			points = '<a tabindex="0" data-html="true" data-toggle="popover" data-placement="left" data-trigger="focus" title="Wartości x" data-content="<strong>na półprostej:</strong><br><strong>od:</strong><br> ' + values.join("<br>") + '">zobacz wartości x</a>';
		} else {
			points = '<a tabindex="0" data-html="true" data-toggle="popover" data-placement="left" data-trigger="focus" title="Wartości x" data-content="<strong>na odcinku:</strong><br><strong>od:</strong><br> ' + values.join("<br>") + '<br><strong>do:</strong><br> ' + secondPointValues.join("<br>") + '">zobacz wartości x</a>';
		}
	}
	writeInConsole(statement + points + '<br>');

	return solution;
}