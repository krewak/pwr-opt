# README #

### Kto? ###
Autor aplikacji: Krzysztof Rewak

Automatyka i robotyka, studia stacjonarne II stopnia,

Wydział Elektroniki, Politechnika Wrocławska

### Dla kogo? ###
Projekt na kurs: Teoria i metody optymalizacji

prowadząca: dr inż. Ewa Szlachcic

### Jak? ###
Wykorzystano język Javascript i framework Backbone.js

### Co? ###
Przedstawiony pogram rozwiązuje zadanie liniowego programowania całkowitoliczbowego w oparciu o dwufazowy algorytm simpleks z wykorzystaniem metody podziału i ograniczeń. Automatycznie załadowane dane pozwalają na uruchomienie progamu, który w pierwszej fazie PL da wynik niecałkowitoliczbowy, a następnie obliczy rozwiązanie PCL. W konsoli komunikatów po prawej stronie ekranu można prześledzić historię działania algorytmu wraz z wszystkimi wynikami pośrednimi.

### Co i jak? ###
Szkielet aplikacji znajduje się w pliku index.html w głównym katalogu. Pliki *.json przechowują automatycznie ładowane do aplikacji informacje dotyczące funkcji celu oraz ograniczeń.

Interfejs znajduje się w js/mcv.js, metoda simpleks w js/simplex.js, a metoda podziału i ograniczeń w js/bb.js

W folderach /css, /fonts i /js/vendor znajdują się potrzebne do uruchomienia aplikacji biblioteki.

### Gdzie? ###
Aplikację może przetestować pod adresem
http://pwr.rewak.eu/timo/